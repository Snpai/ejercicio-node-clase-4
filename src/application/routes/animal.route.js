const express = require('express');
const router = express.Router();
const animalController = require('../controllers/animalController');

router.get('/all',animalController.getAnimals);
router.get('/byspecies/:species',animalController.getBySpecies);
router.get('/:id',animalController.getAnimal);
router.post('/',animalController.saveAnimal);
router.put('/:id',animalController.updateAnimal);
router.delete('/:id',animalController.deleteAnimal);

module.exports = router;