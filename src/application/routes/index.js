const express = require('express');
const router = express.Router();

router.use('/animal', require('./animal.route.js'));


module.exports = router;