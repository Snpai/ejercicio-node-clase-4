const animalBl = require('../../domain/bl/animal.bl.js');
const AnimalSchema = require('../../domain/schemas/animal.schema');
const AnimalModel = require('../../domain/models/animal.model');

function validation (object,res,message) {
     console.log(object);
     if (object) {
          res.status(200).json({
               ok: true,
               message: `the action of ${message} its ok`,
               object
          });
     } else{
          res.status(500).json({
               ok: false,
               message: `error in the ${message}`
          });
     }
}

const animalController = {

     saveAnimal: async function(req,res) {
          const animal = await new AnimalModel(req.body);
          const result = await animalBl.add(animal);
          validation(result,res,'animal save');          
     },
     getAnimals: async (req,res) => {
          const animals = await animalBl.getAll();
          validation(animals,res,'get animals');
     },
     getBySpecies: async (req,res) => {
          const animals = await animalBl.getSpecies(req.params.species);
          validation(animals,res,'get animals by species');
     },
     getAnimal: async (req,res) => {
          const animal = await animalBl.getOne(req.params.id);
          validation(animal,res,'get one animal');
     },
     updateAnimal: async (req,res) => {
          const animal = req.body;
          if (!AnimalSchema.validate(animal).error) {
               const animalValid = await animalBl.update(animal,req.params.id);
               validation(animalValid,res,'animal update');
          }else {
               res.status(500).json({
                    ok: false,
                    message: 'error in the body uwu'
               });
          }

     },
     deleteAnimal: async (req,res) => {
          const result = await animalBl.delete(req.params.id);
          validation(result,res,'deleted animal');
     }
};

module.exports = animalController;