const animalSchema = require('../models/animal.model.js');

const animalBl = () => {
     const scope = this;

     scope.add = async (animal) => {
          try {
               const newAnimal = await new animalSchema(animal);
               await newAnimal.save();
               return newAnimal;
          } catch (e) {
               return null;
          }
     };
     scope.getAll = async () => {
          try {
               const animals = await animalSchema.find();
               return animals;
          } catch(e) {
               return null;
          }
     };
     scope.getSpecies = async (info) => {
          try {
               const animals = await animalSchema.find({species: info});
               return animals;
          } catch(e) {
               return null;
          }          
     };
     scope.getOne = async (id) => {
          try{
               const info = await animalSchema.findById(id);
               return info;
          } catch(e) {
               return null;
          }
     };
     scope.update = async (animal,id) => {
          try{
               await animalSchema.findByIdAndUpdate(id, animal);
               const result = await animalSchema.findById(id);
               return result;
          } catch(e){
               return null;
          }
     };
     scope.delete = async (id) => {
          try{
               await animalSchema.findByIdAndDelete(id);
               return true;
          } catch(e) {
               return null;
          }
     };
     return scope;
};

module.exports = animalBl();