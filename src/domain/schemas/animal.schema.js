const Joi = require('@hapi/joi');
const animalSchema = Joi.object({
     name: Joi
          .string()
          .required()
          .min(3),
     age: Joi
          .number()
          .required()
          .min(0)
          .max(500),
     race: Joi
          .string()
          .required(),
     species: Joi
          .string()
          .valid('DOG','CAT')
          .required()
});
// function newJoi (name,age,species,race) {
//      const newJoi = new animalSchema({
//           name,age,species,race
//      });
//      return newJoi;
// }
module.exports = animalSchema;