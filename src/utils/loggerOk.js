const { createLogger, format, transports } = require('winston');
const moment = require('moment');


module.exports = createLogger({
     format: format.combine(  format.simple(),
                              format.timestamp(),
                              format.printf(info => `${moment(info.timestamp).format('MMMM YYYY')} ${info.level} ${info.message}`)),     
     transports: [
          new transports.File({
               maxsize: 5120000,
               maxFiles: 5,
               filename: `${__dirname}/../logs/validaciones_ok.log`
          }),
          new transports.Console({
               level: 'debug'
          })
     ]
})
