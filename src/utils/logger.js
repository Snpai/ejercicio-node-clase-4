const { createLogger, format, transports } = require('winston');
const moment = require('moment');


module.exports = createLogger({
     format: format.combine(  format.simple(),
                              format.timestamp(),
                              format.printf(info => `${moment(info.timestamp).format('Do MMMM HH:mm')} ${info.level} ${info.message}`)),     
     transports: [
          new transports.File({
               maxsize: 5120000,
               maxFiles: 5,
               filename: `${__dirname}/../logs/validation_errors.log`
          }),
          new transports.Console({
               level: 'debug'
          })
     ]
})
