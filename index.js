const express = require('express');
const app = express();
const mongoose = require('mongoose');
const https = require('https');
const fs = require('fs');
require('dotenv').config();

app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use('/',require('./src/application/routes'));


mongoose.connect(`${process.env.HOST}${process.env.BD}` , { useNewUrlParser: true, useCreateIndex: true },
     (err) => {
          if (err) throw err;
          console.log('BD ON');
     }
);
try{
     https.createServer({
          key: fs.readFileSync('animalitos.key'),
          cert: fs.readFileSync('animalitos.crt')
     }, app).listen(4001, function(){
          console.log('My https server listening on port 4001');
     });     
}catch(e){
     console.log('Error in https');
}
 
     

app.listen(process.env.PORT,() => {
     console.log(`listening in the port ${process.env.PORT}`);
});